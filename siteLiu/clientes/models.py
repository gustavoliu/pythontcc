from django.db import models
from django.contrib.auth.models import User

# Create your models here.


# class Estado(models.Model):
#     sigla = models.CharField(max_length=2)
#     nome = models.CharField(max_length=50)

#     def __str__(self):
#         return self.sigla + " - " + self.nome


# class Cidade(models.Model):
#     nome = models.CharField(max_length=50)
#     estado = models.ForeignKey(Estado, on_delete=models.PROTECT)

#     def __str__(self):
#         return self.nome + " - " + self.estado.sigla


unidade_federativa = (
    ('AC', 'AC'),
    ('AL', 'AL'),
    ('AM', 'AM'),
    ('AP', 'AP'),
    ('BA', 'BA'),
    ('DF', 'DF'),
    ('ES', 'ES'),
    ('GO', 'GO'),
    ('MA', 'MA'),
    ('MG', 'MG'),
    ('MS', 'MS'),
    ('MT', 'MT'),
    ('PA', 'PA'),
    ('PB', 'PB'),
    ('PE', 'PE'),
    ('PI', 'PI'),
    ('PR', 'PR'),
    ('RJ', 'RJ'),
    ('RN', 'RN'),
    ('RO', 'RO'),
    ('RR', 'RR'),
    ('RS', 'RS'),
    ('SC', 'SC'),
    ('SE', 'SE'),
    ('SP', 'SP'),
    ('TO', 'TO'),
)
sexo = (
    ("Masculino", "Masculino"),
    ("Feminino", "Feminino")
)

# Pessoa serão os clientes
class Pessoa(models.Model):

    nome = models.CharField(
        max_length=50, verbose_name="Nome", help_text="Digite seu nome completo")
    nascimento = models.DateField(
        verbose_name='Data de nascimento', help_text="Data de nascimento")
    sexo = models.CharField(
        max_length=10, verbose_name="Sexo", choices=sexo, help_text="Sexo")
    estado = models.CharField(
        max_length=10, verbose_name="Estado", choices=unidade_federativa, help_text="UF")
    cidade = models.CharField(
        max_length=100, verbose_name="Cidade", help_text="Cidade")
    email = models.CharField(
        max_length=100, verbose_name="E-mail", help_text="E-mail")
    telefone = models.CharField(
        max_length=20, verbose_name="Telefone", help_text="Fixo ou celular")

    # cidade = models.ForeignKey(Cidade, on_delete=models.PROTECT)

    def __str__(self):
        return self.nome


class SalaoServico(models.Model):

    nome = models.CharField(
        max_length=50, verbose_name="Serviço", help_text="Título do serviço")
    preco = models.FloatField(verbose_name="Preço")
    minutos = models.FloatField(verbose_name="Tempo de execução do serviço")
    descricao = models.CharField(max_length=120, verbose_name="Descrição")

    def __str__(self):
        return self.nome


class Agendamento(models.Model):

    servico = models.ForeignKey(SalaoServico, on_delete=models.CASCADE)
    preco = models.FloatField(verbose_name="Preço")
    dataAgendamento = models.DateField(verbose_name='Data do agendamento')
    valorPago = models.FloatField(verbose_name="Valor pago", blank=True)
    cliente = models.ForeignKey(Pessoa, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.cliente + '", ID: ' + str(self.dataAgendamento)
