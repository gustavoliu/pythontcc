from django.urls import path, include
from .views import *
from django.urls import path
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', IndexView.as_view(), name="inicio"),
    path('sobre/', SobreView.as_view(), name="sobre"),
    path('curriculo/', CurriculoView.as_view(), name="curriculo"),

    # URLS de cadastros
    path('cadastrar/pessoa/', PessoaCreate.as_view(), name="cadastrar-pessoa"),
    path('cadastrar/servico/', SalaoServicoCreate.as_view(),
         name="cadastrar-servico"),
    path('cadastrar/agendamento/', AgendamentoCreate.as_view(),
         name="cadastrar-agendamento"),

    # URLS de updates
    path('atualizar/pessoa/<int:pk>/',
         PessoaUpdate.as_view(), name="atualizar-pessoa"),
    path('atualizar/servico/<int:pk>/',
         SalaoServicoUpdate.as_view(), name="atualizar-servico"),
    path('atualizar/agendamento/<int:pk>/',
         AgendamentoUpdate.as_view(), name="atualizar-agendamento"),

    # URLS de delete
    path('excluir/pessoa/<int:pk>/', PessoaDelete.as_view(), name="deletar-pessoa"),
    path('excluir/servico/<int:pk>/',
         SalaoServicoDelete.as_view(), name="deletar-servico"),
    path('excluir/agendamento/<int:pk>/',
         AgendamentoDelete.as_view(), name="deletar-agendamento"),

    # URLS de listar

    path('listas/pessoa', PessoaList.as_view(), name="listar-pessoa"),
    path('listas/servico', SalaoServicoList.as_view(), name="listar-servico"),
    path('listas/agendamento', AgendamentoList.as_view(),
         name="listar-agendamento"),

    # URL LOGIN
    path('login/', auth_views.LoginView.as_view(
        template_name='usuarios/login.html',
        extra_context={'titulo': 'Login'}
    ), name='login'),

    # URL Cadastro Funcionário
     path('registrar/funcionario', FuncionarioCreateView.as_view(), name='registroFuncionario'),

    # teste
    path('fctest', FCTesteView.as_view(), name="fctest"),


]
