from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegistroFuncionarioForm(UserCreationForm):
    first_name = forms.CharField(max_length=120, label='Nome')
    last_name = forms.CharField(max_length=120, label='Sobrenome')
    email = forms.EmailField(max_length=254, help_text='E-mail é obrigatório.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        del self.fields['password1']
        del self.fields['password2']
        