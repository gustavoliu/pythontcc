from django.shortcuts import render

# Método que busca um objeto. Se não existir, da um erro 404
from django.shortcuts import get_object_or_404


# Importa todas as classes do models.py
from .models import *

# função que vai chamar as urls pelo "name" delas
from django.urls import reverse_lazy

# as classes views para inserir alterar e excluir
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

# Importa o TemplateView para criação de páginas simples
from django.views.generic import TemplateView

# Importa o controle de acesso de paginas
from django.contrib.auth.mixins import LoginRequiredMixin

# Importa o metodo ListView
from django.views.generic.list import ListView


# Registro funcionário
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import RegistroFuncionarioForm
from django.contrib.auth.models import User, Group

import random
import string

# Controle de grupos
from braces.views import GroupRequiredMixin

# Configuração enviar e-mail com senha
from django.core.mail import send_mail
from django.conf import settings

# Create your views here.


# View registro funcionário
class FuncionarioCreateView(GroupRequiredMixin, LoginRequiredMixin, FormView):
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('inicio')
    form_class = RegistroFuncionarioForm
    # controle de grupos
    group_required = [u"Administrador"]

    def form_valid(self, form):
        # Cria uma "biblioteca" de letras e números
        password_characters = string.ascii_letters + string.digits
        # Pega caracteres aleatórios da biblioteca acima - 8 caracteres
        senha = ''.join(random.choice(password_characters)
                        for i in range(8))
        
        url = super().form_valid(form)
        grupo = Group.objects.get(name=u"Funcionário")
        self.object.groups.add(grupo)
        self.object.password = senha
        self.object.save()

        subject = 'Liu Style - Cadastro Efetuado com Sucesso'
        message = '\n Sua senha é: \n' + senha + \
            "\n Você poderá alterá-la dentro sistema."
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [self.object.email]

        send_mail(subject, message, email_from, recipient_list)

        # Transforma em superuser - usar somente para criar admin
        # self.object.is_staff=True
        # self.object.is_superuser=True

        return url

    def get_context_data(self, *args, **kwargs):
        context = super(FuncionarioCreateView, self).get_context_data(
            *args, **kwargs)
        context['titulo'] = 'Novo Funcionário'
        return context


# def registro(request):
#     if request.method == 'POST':
#         form = RegistroFuncionarioForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             raw_password = form.cleaned_data.get('password1')
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect('inicio')
#     else:
#         form = RegistroFuncionarioForm()
#     return render(request, 'clientes/formulario.html', {'form': form})


class IndexView(TemplateView):
    template_name = "clientes/inicio.html"


class FCTesteView(TemplateView):
    template_name = "clientes/FCTest.html"


class SobreView(TemplateView):
    template_name = "clientes/sobre.html"


class CurriculoView(LoginRequiredMixin, TemplateView):
    template_name = "clientes/curriculo.html"
    ############################################################### INSERIR ################################################################


# class EstadoCreate(LoginRequiredMixin, CreateView):
#     # Define qual o modelo para esta classe
#     model = Estado
#     # Qual o html será utilizado?
#     template_name = "clientes/formulario.html"
#     # Pra onde redirecionar o usuário depoís de inserir um registro. Informe o Informe
#     success_url = reverse_lazy("listar-estado")
#     # Quais campos devem aparecer no formulario?
#     fields = ['sigla', 'nome']

#     def get_context_data(self, *args, **kwargs):
#         context = super(EstadoCreate, self).get_context_data(
#             *args, **kwargs)
#         context['titulo'] = 'Cadastrar Estado'
#         return context


# class CidadeCreate(LoginRequiredMixin, CreateView):
#     model = Cidade
#     fields = ['nome', 'estado']
#     template_name = 'clientes/formulario.html'
#     success_url = reverse_lazy('listar-cidade')

#     def get_context_data(self, *args, **kwargs):
#         context = super(CidadeCreate, self).get_context_data(
#             *args, **kwargs)
#         context['titulo'] = 'Cadastrar Cidade'
#         return context


class PessoaCreate(GroupRequiredMixin, LoginRequiredMixin, CreateView):
    model = Pessoa
    fields = ['nome', 'telefone', 'sexo',
              'nascimento', 'email', 'cidade', 'estado']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-pessoa')
    # controle de grupos
    group_required = [u"Administrador", u"Funcionário"]

    def get_context_data(self, *args, **kwargs):
        context = super(PessoaCreate, self).get_context_data(
            *args, **kwargs)
        context['titulo'] = 'Cadastrar Pessoa'
        return context


class SalaoServicoCreate(GroupRequiredMixin, LoginRequiredMixin, CreateView):
    model = SalaoServico
    fields = ['nome', 'preco', 'minutos', 'descricao']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-servico')
    group_required = u"Administrador"

    def get_context_data(self, *args, **kwargs):
        context = super(SalaoServicoCreate, self).get_context_data(
            *args, **kwargs)
        context['titulo'] = 'Cadastrar Serviço'
        return context


class AgendamentoCreate(GroupRequiredMixin, LoginRequiredMixin, CreateView):
    model = Agendamento
    fields = ['servico', 'preco', 'dataAgendamento',
              'valorPago', 'cliente', 'status']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-agendamento')
    group_required = [u"Administrador", u"Funcionário"]

    def form_valid(self, form):

        # Define o usuário como usuário logado
        form.instance.usuario = self.request.user

        url = super().form_valid(form)

        # código a fazer depois de salvar objeto no banco
        # self.object.atributo = “algo”

        # Salva o objeto novamente
        self.object.save()

        return url


# # UPDATE
# class EstadoUpdate(LoginRequiredMixin, UpdateView):
#     # Define qual o modelo para esta classe
#     model = Estado
#     # Qual o html será utilizado?
#     template_name = "clientes/formulario.html"
#     # Pra onde redirecionar o usuário depoís de inserir um registro. Informe o Informe
#     success_url = reverse_lazy("listar-estado")
#     # Quais campos devem aparecer no formulario?
#     fields = ['sigla', 'nome']


# class CidadeUpdate(LoginRequiredMixin, UpdateView):
#     model = Cidade
#     fields = ['nome', 'estado']
#     template_name = 'clientes/formulario.html'
#     success_url = reverse_lazy('listar-cidade')


class PessoaUpdate(GroupRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Pessoa
    fields = ['nome', 'telefone', 'nascimento', 'email', 'cidade']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-pessoa')
    group_required = [u"Administrador", u"Funcionário"]


class SalaoServicoUpdate(GroupRequiredMixin, LoginRequiredMixin, UpdateView):
    model = SalaoServico
    fields = ['nome', 'preco', 'minutos', 'descricao']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-servico')
    group_required = [u"Administrador"]


class AgendamentoUpdate(GroupRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Agendamento
    fields = ['servico', 'preco', 'dataAgendamento',
              'valorPago', 'cliente', 'status']
    template_name = 'clientes/formulario.html'
    success_url = reverse_lazy('listar-agendamento')
    group_required = [u"Administrador", u"Funcionário"]

    def form_valid(self, form):

        # Define o usuário como usuário logado
        form.instance.usuario = self.request.user

        url = super().form_valid(form)

        # código a fazer depois de salvar objeto no banco
        # self.object.atributo = “algo”

        # Salva o objeto novamente
        self.object.save()

        return url

    # Altera a query para buscar o objeto do usuário logado
    def get_object(self, queryset=None):
        self.object = get_object_or_404(
            Agendamento, pk=self.kwargs['pk'], usuario=self.request.user)
        return self.object


# DELETE


# class EstadoDelete(LoginRequiredMixin, DeleteView):
#     # Define qual o modelo para esta classe
#     model = Estado
#     # Qual o html será utilizado?
#     template_name = "clientes/formularioExcluir.html"
#     # Pra onde redirecionar o usuário depoís de inserir um registro. Informe o Informe
#     success_url = reverse_lazy("listar-estado")
#     # Quais campos devem aparecer no formulario?
#     fields = ['sigla', 'nome']


# class CidadeDelete(LoginRequiredMixin, DeleteView):
#     model = Cidade
#     fields = ['nome', 'estado']
#     template_name = 'clientes/formularioExcluir.html'
#     success_url = reverse_lazy('listar-cidade')


class PessoaDelete(LoginRequiredMixin, DeleteView):
    model = Pessoa
    template_name = 'clientes/formularioExcluir.html'
    success_url = reverse_lazy('listar-pessoa')


class SalaoServicoDelete(LoginRequiredMixin, DeleteView):
    model = SalaoServico
    template_name = 'clientes/formularioExcluir.html'
    success_url = reverse_lazy('listar-servico')


class AgendamentoDelete(LoginRequiredMixin, DeleteView):
    model = Agendamento
    template_name = 'clientes/formularioExcluir.html'
    success_url = reverse_lazy('listar-agendamento')

    # Altera a query para buscar o objeto do usuário logado

    def get_object(self, queryset=None):
        self.object = get_object_or_404(
            Agendamento, pk=self.kwargs['pk'], usuario=self.request.user)
        return self.object


# # LISTAR

# class EstadoList(LoginRequiredMixin, ListView):
#     model = Estado
#     template_name = 'clientes/listas/listar-estado.html'


# class CidadeList(LoginRequiredMixin, ListView):
#     model = Cidade
#     template_name = 'clientes/listas/listar-cidade.html'


class PessoaList(LoginRequiredMixin, ListView):
    model = Pessoa
    template_name = 'clientes/listas/listar-pessoa.html'


class SalaoServicoList(LoginRequiredMixin, ListView):
    model = SalaoServico
    template_name = 'clientes/listas/listar-servico.html'


class AgendamentoList(LoginRequiredMixin, ListView):
    model = Agendamento
    template_name = 'clientes/listas/listar-agendamento.html'

    # Método que permite listar apenas os agendamentos de determinado usuário

    def get_queryset(self):
        # O object_list armazena uma lista de objetos de um ListView
        self.object_list = Agendamento.objects.filter(
            usuario=self.request.user)
        return self.object_list
