from django.urls import path
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(
        template_name='usuarios/login.html',
        extra_context={'titulo': 'Login'}
    ), name='login'),


    path('sair/', auth_views.LogoutView.as_view(), name="logout"),

    path('alterar-minha-senha/', auth_views.PasswordChangeView.as_view(
        template_name='usuarios/login.html',
        extra_context={'titulo': 'Alterar senha atual'},
    ), name="alterar-senha"),

    # REDIREÇÃO PARA QUANDO A SENHA FOR ALTERADA COM SUCESSO
    path('alterar-minha-senha/pronto/', auth_views.PasswordChangeDoneView.as_view(
        template_name='usuarios/alterar-senha-sucesso.html'), name='password_change_done'),

]
